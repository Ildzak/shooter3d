using UnityEngine;

namespace Client
{
    struct PushEvent
    {
        public Rigidbody PushedBody;
        public Vector3 Direction;
        public float Force;
    }
}