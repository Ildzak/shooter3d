using UnityEngine;

namespace Client
{
    struct TouchEvent
    {
        public TouchPhase TouchPhase;
        public Vector3 DeltaPosition;
        public Vector3 TouchPos;
        public Vector3 LastTouchPosAfterEnded;
    }

    public enum TouchPhase
    {
        Began = 1, Moved = 2, Stationary = 3, Ended = 4
    }
}