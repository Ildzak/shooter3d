using UnityEngine;

namespace Client
{
    struct ChangePositionComponent
    {
        public Vector3 CurrentPointPos;
        public Vector3 NextPointPos;
        public float T;
    }
}