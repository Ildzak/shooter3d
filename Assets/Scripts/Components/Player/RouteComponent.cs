using UnityEngine;
using System.Collections.Generic;

namespace Client
{
    struct RouteComponent
    {
        public List<Transform> RoutePoints;
        public List<EnemyMB> CurrentEnemies;
        public int CurrentIndexPoint;
    }
}