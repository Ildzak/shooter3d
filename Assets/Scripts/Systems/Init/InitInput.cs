using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    sealed class InitInput : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            var state = systems.GetShared<State>();

            var entity = w.NewEntity();
            state.EntityInput = entity;

            w.GetPool<InputComponent>().Add(entity);
        }
    }
}