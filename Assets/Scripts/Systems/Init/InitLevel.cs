using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    sealed class InitLevel : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            var state = systems.GetShared<State>();

            var entity = w.NewEntity();
            state.EntityLevel = entity;

            w.GetPool<Level>().Add(entity);
            
            w.GetPool<EnableBeforeStartLevelEvent>().Add(entity);
        }
    }
}