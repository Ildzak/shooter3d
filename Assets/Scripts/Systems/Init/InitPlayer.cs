using Leopotam.EcsLite;
using UnityEngine;
using System.Collections.Generic;

namespace Client
{
    sealed class InitPlayer : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            var state = systems.GetShared<State>();

            var entity = w.NewEntity();
            state.EntityPlayer = entity;

            var playerObj = GameObject.FindGameObjectWithTag("Player");

            w.GetPool<Player>().Add(entity);

            ref var viewComp = ref w.GetPool<View>().Add(entity);
            viewComp.Transform = playerObj.transform;

            ref var routeComp = ref w.GetPool<RouteComponent>().Add(entity);
            var routePoints = GameObject.FindGameObjectsWithTag("Point");
            routeComp.RoutePoints = new List<Transform>();
            for (int i = 0; i < routePoints.Length; i++) routeComp.RoutePoints.Add(routePoints[i].transform);
            var currentEnemies = routeComp.RoutePoints[0].GetComponentsInChildren<EnemyMB>();
            routeComp.CurrentEnemies = new List<EnemyMB>();
            for (int a = 0; a < currentEnemies.Length; a++) routeComp.CurrentEnemies.Add(currentEnemies[a]);
            routeComp.CurrentIndexPoint = 0;
        }
    }
}