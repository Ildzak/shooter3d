using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    sealed class InitEnemy : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            var state = systems.GetShared<State>();

            var enemies = GameObject.FindGameObjectsWithTag("Enemy");

            for (int i = 0; i < enemies.Length; i++)
            {
                var entity = w.NewEntity();

                w.GetPool<Enemy>().Add(entity);

                ref var viewComp = ref w.GetPool<View>().Add(entity);
                viewComp.Transform = enemies[i].transform;
                enemies[i].GetComponent<EnemyMB>().Entity = entity;

                w.GetPool<AliveComponent>().Add(entity);
            }

            ref var routeComp = ref w.GetPool<RouteComponent>().Get(state.EntityPlayer);
            for (int a = 0; a < routeComp.CurrentEnemies.Count; a++)
            {
                ref var canMoveComp = ref w.GetPool<CanMoveComponent>().Add(routeComp.CurrentEnemies[a].Entity);
                canMoveComp.Rigidbody = routeComp.CurrentEnemies[a].GetComponent<Rigidbody>();
            }
        }
    }
}