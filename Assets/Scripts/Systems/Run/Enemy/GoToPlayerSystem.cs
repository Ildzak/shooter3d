using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class GoToPlayerSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<CanMoveComponent> _ecsFilter;
        private EcsPool<View> _viewPool;
        private State _state;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _viewPool = w.GetPool<View>();
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var canMoveComp = ref _ecsFilter.Inc1().Get(entity);
                ref var playerViewComp = ref _viewPool.Get(_state.EntityPlayer);

                var direction = playerViewComp.Transform.position - canMoveComp.Rigidbody.transform.position;

                canMoveComp.Rigidbody.velocity = direction * (_state.EnemyConfig.Speed * Time.deltaTime);

                var layer = LayerMask.GetMask("Player");
                var checkPlayer = Physics.OverlapSphere(canMoveComp.Rigidbody.transform.position, 2f, layer);
                if (checkPlayer.Length > 0)
                {
                    _ecsFilter.Inc1().Del(entity);
                }
            }
        }
    }
}