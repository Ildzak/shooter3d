using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class DeadSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<DeadEvent> _ecsFilter;
        private EcsPool<AliveComponent> _alivePool;
        private EcsPool<DeadComponent> _deadPool;
        private EcsPool<RouteComponent> _routePool;
        private EcsPool<CanMoveComponent> _canMovePool;
        private State _state;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _alivePool = w.GetPool<AliveComponent>();
            _deadPool = w.GetPool<DeadComponent>();
            _routePool = w.GetPool<RouteComponent>();
            _canMovePool = w.GetPool<CanMoveComponent>();
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                _alivePool.Del(entity);
                _deadPool.Add(entity);

                ref var routeComp = ref _routePool.Get(_state.EntityPlayer);
                for (int i = 0; i < routeComp.CurrentEnemies.Count; i++)
                {
                    if (routeComp.CurrentEnemies[i].Entity == entity)
                    {
                        if (_canMovePool.Has(entity))
                        {
                            _canMovePool.Del(entity);
                            var enemyRigidbody = routeComp.CurrentEnemies[i].GetComponent<Rigidbody>();
                            enemyRigidbody.freezeRotation = false;
                        }
                        routeComp.CurrentEnemies[i].GetComponent<MeshRenderer>().material = _state.EnemyConfig.DeadMaterial;
                        routeComp.CurrentEnemies.RemoveAt(i);
                        break;
                    }
                }
            }
        }
    }
}