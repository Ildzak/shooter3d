using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class ChangePositionSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<ChangePositionComponent> _ecsFilter;
        private EcsPool<View> _viewPool;
        private EcsPool<RouteComponent> _routePool;
        private EcsPool<CanMoveComponent> _canMovePool;
        private State _state;
        private float _debounceTimer;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _viewPool = w.GetPool<View>();
            _routePool = w.GetPool<RouteComponent>();
            _canMovePool = w.GetPool<CanMoveComponent>();
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                _debounceTimer += Time.deltaTime;
                if (_debounceTimer > _state.PlayerConfig.ChangePosTimer)
                {
                    ref var changePosComp = ref _ecsFilter.Inc1().Get(entity);
                    ref var viewComp = ref _viewPool.Get(entity);

                    changePosComp.T = changePosComp.T + (_state.PlayerConfig.Speed * Time.deltaTime);
                    viewComp.Transform.position = Vector3.Lerp(changePosComp.CurrentPointPos, changePosComp.NextPointPos, changePosComp.T);

                    if (changePosComp.T > 1f)
                    {
                        ref var routeComp = ref _routePool.Get(entity);
                        routeComp.CurrentIndexPoint++;
                        var currentEnemies = routeComp.RoutePoints[routeComp.CurrentIndexPoint].GetComponentsInChildren<EnemyMB>();
                        for (int i = 0; i < currentEnemies.Length; i++) routeComp.CurrentEnemies.Add(currentEnemies[i]);

                        for (int a = 0; a < routeComp.CurrentEnemies.Count; a++)
                        {
                            ref var canMoveComp = ref _canMovePool.Add(routeComp.CurrentEnemies[a].Entity);
                            canMoveComp.Rigidbody = routeComp.CurrentEnemies[a].GetComponent<Rigidbody>();
                        }

                        _ecsFilter.Inc1().Del(entity);
                    }
                }
            }
        }
    }
}