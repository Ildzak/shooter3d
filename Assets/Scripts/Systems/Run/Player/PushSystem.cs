using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class PushSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<PushEvent> _ecsFilter;
        private EcsPool<DeadEvent> _deadEventPool;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _deadEventPool = w.GetPool<DeadEvent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var pushEvent = ref _ecsFilter.Inc1().Get(entity);

                pushEvent.PushedBody.AddForce(pushEvent.Direction * pushEvent.Force, ForceMode.Impulse);

                int pushedEntity = pushEvent.PushedBody.GetComponent<EnemyMB>().Entity;
                _deadEventPool.Add(pushedEntity);
            }
        }
    }
}