using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class ForceBuffSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<TouchEvent> _ecsFilter;
        private EcsPool<ForceBuffComponent> _forceBuffPool;
        private State _state;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _forceBuffPool = w.GetPool<ForceBuffComponent>();
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var touchEvent = ref _ecsFilter.Inc1().Get(entity);

                if (touchEvent.TouchPhase == TouchPhase.Moved || touchEvent.TouchPhase == TouchPhase.Stationary)
                {
                    if (!_forceBuffPool.Has(_state.EntityPlayer)) _forceBuffPool.Add(_state.EntityPlayer);
                    ref var forceBuffComp = ref _forceBuffPool.Get(_state.EntityPlayer);

                    if (forceBuffComp.ForceBuff == 0) forceBuffComp.ForceBuff = _state.PlayerConfig.MinPushForce;

                    if (forceBuffComp.ForceBuff < _state.PlayerConfig.MaxPushForce)
                        forceBuffComp.ForceBuff = forceBuffComp.ForceBuff + (1 * Time.deltaTime);
                }
            }
        }
    }
}