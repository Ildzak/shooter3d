using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class CheckRaycastEnemySystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<TouchEvent> _ecsFilter;
        private EcsPool<View> _viewPool;
        private EcsPool<PushEvent> _pushPool;
        private EcsPool<ForceBuffComponent> _forceBuffPool;
        private State _state;
        private float _checkTimer;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _viewPool = w.GetPool<View>();
            _pushPool = w.GetPool<PushEvent>();
            _forceBuffPool = w.GetPool<ForceBuffComponent>();
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var touchEvent = ref _ecsFilter.Inc1().Get(entity);

                if (touchEvent.TouchPhase == TouchPhase.Ended)
                {
                    ref var viewComp = ref _viewPool.Get(_state.EntityPlayer);

                    var screenPos = Camera.main.ScreenPointToRay(touchEvent.LastTouchPosAfterEnded);

                    LayerMask layer = LayerMask.GetMask("Enemy");
                    RaycastHit hit;
                    if (Physics.Raycast(viewComp.Transform.position, screenPos.direction, out hit, 100f, layer))
                    {
                        float forceBuff = _state.PlayerConfig.MinPushForce;
                        if (_forceBuffPool.Has(_state.EntityPlayer))
                        {
                            forceBuff = _forceBuffPool.Get(_state.EntityPlayer).ForceBuff;
                            _forceBuffPool.Del(_state.EntityPlayer);
                        }

                        ref var pushEvent = ref _pushPool.Add(_state.EntityPlayer);
                        pushEvent.PushedBody = hit.rigidbody;
                        pushEvent.Direction = hit.transform.position - viewComp.Transform.position;
                        pushEvent.Force = forceBuff;
                    }

                    _ecsFilter.Inc1().Del(entity);
                }
            }
        }
    }
}