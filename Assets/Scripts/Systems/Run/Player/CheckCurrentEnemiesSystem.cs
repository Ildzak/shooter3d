using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class CheckCurrentEnemiesSystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<RouteComponent>.Exc<ChangePositionComponent> _ecsFilter;
        private EcsPool<ChangePositionComponent> _changePosPool;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _changePosPool = w.GetPool<ChangePositionComponent>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var routeComp = ref _ecsFilter.Inc1().Get(entity);

                if (routeComp.CurrentEnemies.Count == 0 && routeComp.CurrentIndexPoint != routeComp.RoutePoints.Count - 1)
                {
                    ref var changePosComp = ref _changePosPool.Add(entity);
                    changePosComp.CurrentPointPos = routeComp.RoutePoints[routeComp.CurrentIndexPoint].position;
                    changePosComp.NextPointPos = routeComp.RoutePoints[routeComp.CurrentIndexPoint + 1].position;
                }
            }
        }
    }
}