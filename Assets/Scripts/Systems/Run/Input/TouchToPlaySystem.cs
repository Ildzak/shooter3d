using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class TouchToPlaySystem : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<TouchEvent> _ecsFilter;
        private State _state;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
            _state = systems.GetShared<State>();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                ref var touchEvent = ref _ecsFilter.Inc1().Get(entity);

                if (touchEvent.TouchPhase == TouchPhase.Began)
                {
                    _state.World.GetPool<EnablePlayLevelEvent>().Add(_state.EntityLevel);
                }
            }
        }
    }
}