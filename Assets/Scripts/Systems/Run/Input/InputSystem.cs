using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using InputTouchPhase = UnityEngine.InputSystem.TouchPhase;

namespace Client
{
    sealed class InputSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        private EcsFilterExt<InputComponent> _filter;
        private EcsPool<TouchEvent> _touchPool = null;
        private State _state = null;
        private float _stationaryTimer, _debounceTimer;


        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();

            EnhancedTouchSupport.Enable(); // активировали ввод
            TouchSimulation.Enable(); // клик будет симулировать touch

            _touchPool = w.GetPool<TouchEvent>();
            _filter.Validate(w);
        }

        public void Destroy(EcsSystems systems)
        {
            EnhancedTouchSupport.Disable();
            TouchSimulation.Disable();
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter.Filter())
            {
                if (Touch.activeTouches.Count == 0) return;
                var activeTouch = Touch.activeTouches[0];

                var phase = activeTouch.phase;
                if (phase == InputTouchPhase.Began)
                {
                    // если фаза Began - начать отсчет
                    _debounceTimer = 0;
                    if (!_touchPool.Has(entity))
                    {
                        _touchPool.Add(entity);
                    }
                    ref var touchComp = ref _touchPool.Get(entity);
                    touchComp.TouchPhase = TouchPhase.Began;
                    touchComp.DeltaPosition = Vector3.zero;
                    touchComp.TouchPos = activeTouch.screenPosition;
                    touchComp.LastTouchPosAfterEnded = Vector3.zero;
                    return;
                }

                if (phase == InputTouchPhase.Ended || phase == InputTouchPhase.Canceled)
                {
                    _debounceTimer = 0;
                    if (!_touchPool.Has(entity))
                    {
                        _touchPool.Add(entity);
                    }
                    ref var touchComp = ref _touchPool.Get(entity);
                    touchComp.TouchPhase = TouchPhase.Ended;
                    touchComp.DeltaPosition = Vector3.zero;
                    touchComp.LastTouchPosAfterEnded = touchComp.TouchPos;
                    touchComp.TouchPos = Vector3.zero;
                    return;
                }

                _debounceTimer += Time.deltaTime;
                if (_debounceTimer < 0.2f /*_state.Config.InputDebounceTime*/) return;

                if (phase == InputTouchPhase.Moved)
                {
                    _stationaryTimer = 0;

                    if (!_touchPool.Has(entity)) _touchPool.Add(entity);
                    ref var touchComp = ref _touchPool.Get(entity);

                    if (activeTouch.delta.x > 1f /*_state.Config.MinDeltaX*/)
                    {
                        touchComp.TouchPhase = TouchPhase.Moved;
                        touchComp.DeltaPosition.x = 1;
                        touchComp.TouchPos = activeTouch.screenPosition;
                        return;
                    }
                    else if (activeTouch.delta.x < -1f /*-_state.Config.MinDeltaX*/)
                    {
                        touchComp.TouchPhase = TouchPhase.Moved;
                        touchComp.DeltaPosition.x = -1;
                        touchComp.TouchPos = activeTouch.screenPosition;
                        return;
                    }
                }
                else if (phase == InputTouchPhase.Stationary)
                {
                    // чтобы не дергался во время медленного движения пальцем
                    // если зашли в состояние Stationary, то не сразу убирать старый Direction, а через 0.2 сек
                    if (!_touchPool.Has(entity)) _touchPool.Add(entity);
                    ref var touchComp = ref _touchPool.Get(entity);
                    _stationaryTimer += Time.deltaTime;
                    if (_stationaryTimer >= 0.2f)
                    {
                        touchComp.TouchPhase = TouchPhase.Stationary;
                        touchComp.DeltaPosition = Vector3.zero;
                        touchComp.TouchPos = activeTouch.screenPosition;
                    }
                }
            }
        }
    }
}
