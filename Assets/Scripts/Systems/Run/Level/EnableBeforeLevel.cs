using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class EnableBeforeLevel : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<EnableBeforeStartLevelEvent> _ecsFilter;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                var w = systems.GetWorld();

                ref var evt = ref w.GetPool<EcsGroupSystemState>().Add(w.NewEntity());
                evt.Name = "BeforeStartLevel";
                evt.State = true;

                _ecsFilter.Inc1().Del(entity);
            }
        }
    }
}