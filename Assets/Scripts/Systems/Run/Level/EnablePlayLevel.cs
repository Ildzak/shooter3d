using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;
using Leopotam.EcsLite.ExtendedFilters;
using UnityEngine;

namespace Client
{
    sealed class EnablePlayLevel : IEcsRunSystem, IEcsInitSystem
    {
        private EcsFilterExt<EnablePlayLevelEvent> _ecsFilter;

        public void Init(EcsSystems systems)
        {
            var w = systems.GetWorld();
            _ecsFilter.Validate(w);
        }

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _ecsFilter.Filter())
            {
                var w = systems.GetWorld();

                ref var evt1 = ref w.GetPool<EcsGroupSystemState>().Add(w.NewEntity());
                evt1.Name = "PlayLevel";
                evt1.State = true;

                ref var evt2 = ref w.GetPool<EcsGroupSystemState>().Add(w.NewEntity());
                evt2.Name = "BeforeStartLevel";
                evt2.State = false;

                _ecsFilter.Inc1().Del(entity);
            }
        }
    }
}