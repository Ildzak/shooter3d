using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;
using UnityEngine;

namespace Client
{
    sealed class EcsStartup : MonoBehaviour
    {
        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private EnemyConfig _enemyConfig;
        private EcsWorld _world;
        private EcsSystems _systems;

        void Start()
        {
            _world = new EcsWorld();
            var _state = new State(_world, _playerConfig, _enemyConfig);

            _systems = new EcsSystems(_world, _state);

            _systems
                .Add(new InitLevel())
                .Add(new InitInput())
                .Add(new InitPlayer())
                .Add(new InitEnemy())
                ;

            _systems
                .Add(new InputSystem())
                ;

            _systems.AddGroup("BeforeStartLevel", false, null,
                new TouchToPlaySystem()
            );

            _systems.AddGroup("PlayLevel", false, null,
                // Player's attack systems
                new ForceBuffSystem(),
                new CheckRaycastEnemySystem(),
                new PushSystem(),
                
                // Player's move systems
                new CheckCurrentEnemiesSystem(),
                new ChangePositionSystem(),

                // Enemies' system
                new GoToPlayerSystem(),
                new DeadSystem()
            );

            _systems
                .Add(new EnableBeforeLevel())
                .Add(new EnablePlayLevel())
                ;

            _systems
                .DelHere<PushEvent>()
                .DelHere<DeadEvent>()
                ;

#if UNITY_EDITOR
            _systems.Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem());
#endif

            _systems.Init();
        }

        void Update()
        {
            _systems?.Run();
        }

        void OnDestroy()
        {
            if (_systems != null)
            {
                _systems.Destroy();
                // add here cleanup for custom worlds, for example:
                // _systems.GetWorld ("events").Destroy ();
                _systems.GetWorld().Destroy();
                _systems = null;
            }
        }
    }
}