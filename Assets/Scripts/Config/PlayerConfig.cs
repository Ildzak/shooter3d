using UnityEngine;

[CreateAssetMenu(fileName = "PlayerConfig", menuName = "Configs/PlayerConfig", order = 0)]
public class PlayerConfig : ScriptableObject
{
    [Header("Push")]
    public float MinPushForce;
    public float MaxPushForce;

    [Header("ChangePosition")]
    public float Speed;

    [Header("Timer")]
    public float ChangePosTimer;
}
