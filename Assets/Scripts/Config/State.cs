using UnityEngine;
using Leopotam.EcsLite;

public class State
{
    public EcsWorld World;

    public PlayerConfig PlayerConfig;
    public EnemyConfig EnemyConfig;
    
    public int EntityLevel;
    public int EntityInput;
    public int EntityPlayer;

    public State(EcsWorld world, PlayerConfig playerConfig, EnemyConfig enemyConfig)
    {
        World = world;
        PlayerConfig = playerConfig;
        EnemyConfig = enemyConfig;
    }
}
