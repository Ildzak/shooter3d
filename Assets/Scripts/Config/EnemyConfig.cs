using UnityEngine;

[CreateAssetMenu(fileName = "EnemyConfig", menuName = "Configs/EnemyConfig", order = 1)]
public class EnemyConfig : ScriptableObject
{
    [Header("Status")]
    public Material AliveMaterial;
    public Material DeadMaterial;
    [Header("Move")]
    public float Speed;
}
