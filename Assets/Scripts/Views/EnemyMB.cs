using UnityEngine;

public class EnemyMB : MonoBehaviour
{
    private int _entity;
    public int Entity
    {
        get { return _entity; }
        set { _entity = value; }
    }
}
